function setHeaderHeight() {
    var h = $(window).height();
    $('header').css({
        height: h
    });
}

$(document).ready(function() {
    setHeaderHeight();
    $(".helpSlider").royalSlider({
        // general options go gere
        autoScaleSlider: true,
        autoHeight: true,
        video: {
            // video options go gere
            autoHideBlocks: true,
            autoHideArrows: false
        }
    });
    $(".t-feed-slider").royalSlider({
        // general options go gere
        autoScaleSlider: true,
        autoHeight: true,
        arrowsNav: true,
        video: {
            // video options go gere
            autoHideBlocks: true,
            autoHideArrows: false
        }
    });
    //$('.rsArrowRight').addClass('fa').addClass('fa-chevron-right');
    var s_height = $('.t-feed').height();
    //console.log(s_height);
});

$( window ).resize(function() {
    setHeaderHeight();
});

$( window ).scroll(function() {
    var a = $(this).scrollTop();
    if ((a > 51)) {
        $(".navbar").addClass('white');
    } else {
        $(".navbar").removeClass('white');
    }
});
